package com.hemebiotech.analytics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class SymptomWriterImpl implements ISymptomWriter{

    /**
     * Write the map result in the file of out.
     * @param mapSymptoms a map of the symptom and are number of appearance
     */
    @Override
    public void writeSymptomsToFile(Map<String ,Integer> mapSymptoms) {
        try {
            File file = new File("../Project02Eclipse/results.out");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            mapSymptoms.forEach((symptoms, nb) -> {
                try {
                    bw.write(symptoms + "=" + nb + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
