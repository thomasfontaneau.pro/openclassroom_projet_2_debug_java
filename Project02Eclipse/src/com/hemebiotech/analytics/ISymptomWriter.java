package com.hemebiotech.analytics;

import java.util.Map;

/**
 * Interface for write in an out file the result
 */
public interface ISymptomWriter {
    void writeSymptomsToFile(Map<String ,Integer> mapSymptoms);
}
