package com.hemebiotech.analytics;

import java.util.List;
import java.util.Map;

/**
 * Interface for count the symptoms in a list
 */
public interface ISymptomCounter {
    Map<String ,Integer> countSymptoms(List<String> symptoms);
}
