package com.hemebiotech.analytics;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadSymptomDataFromFile implements ISymptomReader {

    private String filePath;

    public ReadSymptomDataFromFile(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Read the file and add they name read's in the list.
     * @return a list of symptom
     */
    @Override
    public List<String> getSymptoms() {
        List<String> symptoms = new ArrayList<>();
        try {
            FileInputStream file = new FileInputStream(filePath);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                symptoms.add(scanner.nextLine());
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return symptoms;
    }

    /**
     * Return the value of variable filePath.
     */
    public String getFilePath() { return filePath; }

    /**
     * Change the value of variable filePath.
     * @param filePath the file path
     */
    public void setFilePath(String filePath) { this.filePath = filePath; }
}
