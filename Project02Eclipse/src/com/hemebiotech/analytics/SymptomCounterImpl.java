package com.hemebiotech.analytics;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SymptomCounterImpl implements ISymptomCounter{

    /**
     * Count the number of iterations of each word in the list and add the result in a map.
     * @param symptoms list of symptom
     * @return a map of the symptom and are number of appearance
     */
    @Override
    public Map<String ,Integer> countSymptoms(List<String> symptoms) {
        Map<String, Integer> resultSymptoms = new TreeMap<>();
        symptoms.forEach(symptom -> {
            if (!resultSymptoms.containsKey(symptom)) {
                resultSymptoms.put(symptom, Collections.frequency(symptoms, symptom));
            }
        });
        return resultSymptoms;
    }
}
