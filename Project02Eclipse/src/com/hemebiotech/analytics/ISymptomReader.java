package com.hemebiotech.analytics;

import java.util.List;

/**
 * Interface for read a file and add the symptom in a list
 */
public interface ISymptomReader {
    List<String> getSymptoms();
}
